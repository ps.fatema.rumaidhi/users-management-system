package com.progressoft.jip9.usermanagement;

import java.util.ArrayList;

//TODO rename this class
// TODO introduce packages for your code com.progres.....
public interface DataBase {

    //void displayAllUsers();
    String displayAllUsers();
    void addNewUser(UserData newUser);

    boolean enableUserName(String userName);

    boolean disableUserName(String userName);

    void resetUserPassword(String username, String newPassword);

    boolean validateUserAndEmail(String userName, String email);

    boolean IsExistUserNameOrEmail(String userName, String email, int i);
   // public boolean isExistUserName(String userName);


}

/*
import java.util.ArrayList;

//TODO rename this class
// TODO introduce packages for your code com.progres.....
public interface DataBase {

   void displayAllUsers();

   void addNewUser(UserData newUser);

   boolean enableUserName(String userName);

   boolean disableUserName(String userName);

   void resetUserPassword(String username, String newPassword);

   boolean validateUserAndEmail(String userName, String email);

   boolean IsExistUserNameOrEmail(String userName, String email, int i);

   String getUsers();
}

 */
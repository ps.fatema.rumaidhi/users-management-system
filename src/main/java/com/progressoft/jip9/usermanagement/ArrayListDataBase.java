package com.progressoft.jip9.usermanagement;

import org.apache.commons.lang3.ObjectUtils;

import java.util.ArrayList;
import java.util.Objects;

// TODO this should only responsible for managing the users list
public class ArrayListDataBase implements DataBase {


    private ArrayList<UserData> usersList = new ArrayList<UserData>();
    // private  Output output= new Output();

    public boolean validateUserAndEmail(String userName, String email) {
        for (int i = 0; i < usersList.size(); i++) {
            if (IsExistUserNameOrEmail(userName, email, i))
                return false;
        }
        return true;
    }

    public boolean IsExistUserNameOrEmail(String userName, String email, int i) {
        if (userName.equals(usersList.get(i).getUsername()) || email.equals(usersList.get(i).getEmail())) {
            return true;
        }
        return false;
    }

    public void addNewUser(UserData newUser) throws IllegalArgumentException{
        if (!validateUserAndEmail(newUser.getName(), newUser.getEmail())) {
             throw new IllegalArgumentException("User-name or email is alreay exist");
          //  return false;
        }
        usersList.add(newUser);
        //  return true;
    }
/*
    public void enableUserName(String userName) {
        // TODO you should return a boolean indicating that user is modified or not
        for (int i = 0; i < usersList.size(); i++) {
            if (userName.equals(usersList.get(i).getUsername())) {
                if (usersList.get(i).getActiveState() == true) {
                    // TODO the output shouldn't be here
                    //System.out.println("user already active");
                    output.userIsActive();

                } else {
                    usersList.get(i).setActive(true);
                }
            }
        }
    }

 */

    /*
      public boolean isExistUserName(String userName){
        for (int i = 0; i < usersList.size(); i++) {

            if (!userName.equals(usersList.get(i).getUsername())) {
                return false;
            }

    }
        return true;
    }
     */
    public boolean enableUserName(String userName) {
        // TODO you should return a boolean indicating that user is modified or not

        for (int i = 0; i < usersList.size(); i++) {

            if (userName.equals(usersList.get(i).getUsername())) {
                return checkActiveStatus(i);
      /*
            else{

                 throw new IllegalArgumentException("This user-name does not exist");

            }

             */
            }
        }
            return false;

    }

    private boolean checkActiveStatus(int i){
            if (usersList.get(i).getActiveStatus()) {
                // TODO the output shouldn't be here
                return false;
            }
            usersList.get(i).setActive(true);
            return true;

    }

    public boolean disableUserName(String userName) {
        for (int i = 0; i < usersList.size(); i++) {
            if (userName.equals(usersList.get(i).getUsername())) {
                return checkInActiveStatus(i);
            }

        }
        return false;
    }

    private boolean checkInActiveStatus(int i) {
        if (!usersList.get(i).getActiveStatus()) {
            // TODO same as above
            return false;
        }
        usersList.get(i).setActive(false);
        return true;
    }


    /*
    public void displayAllUsers() {
        if (usersList.size() == 0) {
            //  output.emptyDB();            /!!!!!!!!!!!!!!!!!!!!!!!!!!
            throw new IllegalArgumentException("List is Empty");
        }
        // TODO the process of printing is the same regardless of where the users(ArrayListDataBase) are stored
        //output.displayAll(usersList);
        //return usersList;
    }

     */

    public String displayAllUsers() {
        if (usersList.size() == 0) {

            throw new IllegalArgumentException("List is Empty");
        }
        String users = "";
        // ArrayList<UserData> outputUsers = new ArrayList<UserData>();

        users = getString(users);

        return users;


    }

    private String getString(String users) {
        for (int i = 0; i < usersList.size(); i++) {

            // outputUsers.add(new UserData(usersList.get(i).getUsername(),usersList.get(i).getName(),));

            users += "|        " + usersList.get(i).getUsername();
            users += "    |         " + usersList.get(i).getName();
            users += "   |         " + usersList.get(i).getEmail();


            if (!usersList.get(i).getActiveStatus()) {
                users += "   |       " + "No\n";
            } else {
                users += "   |       " + "Yes\n";
            }
        }
        return users;
    }


    public void resetUserPassword(String username, String newPassword) {
        for (int i = 0; i < usersList.size(); i++) {
            if (username.equals(usersList.get(i).getUsername())) {
                usersList.get(i).setPassword(newPassword);
            }
        }

    }
}

/*
import java.util.ArrayList;

// TODO this should only responsible for managing the users list
public class Users implements UserInterface {

status

*/
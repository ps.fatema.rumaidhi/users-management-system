package com.progressoft.jip9.usermanagement;

import java.util.ArrayList;

public class UserData {


    private String name, username, email, password;
    private boolean active;


    public UserData(String username,String name,  String email, String password, boolean active) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.active = active;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getActiveStatus() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }




}

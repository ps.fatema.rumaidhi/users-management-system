package com.progressoft.jip9.usermanagement;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Objects;
import java.util.Scanner;

public class UsersManagement {
    private DataBase userInterfaceObj;
    private RandomPassword randomPassword= new RandomPassword();

    public UsersManagement(DataBase user2) {
        this.userInterfaceObj = user2;
    }

    protected void displayAllUsers(){
    userInterfaceObj.displayAllUsers();
    }


    protected void exist() {
        System.exit(0);
    }

    protected int ValidateUserChoose(int userChoose) throws IllegalArgumentException {
        boolean trueChoose = userChooseWithinTheRange(userChoose);
        if (!trueChoose) {
            throw new IllegalArgumentException("Please enter a number from 1 - 6 only");
        } else {
            return userChoose;
        }
    }


    private boolean userChooseWithinTheRange(int userChoose) {
        int[] chooses = {1, 2, 3, 4, 5, 6};
        boolean trueChoose = false;
        for (int index = 0; index < 6; index++) {
            if (userChoose == chooses[index]) trueChoose = true;
        }
        return trueChoose;
    }


    // TODO rename the method to start with small letter
    /*
    private void displayMenu() {
        System.out.print("\n1- Display all users\n2- Add new user\n" +
                "3- Enable user\n4- Disable user\n" +
                "5- Reset Password\n" +
                "6- Exit\nPlease enter your option:\n ");
    }

     */

    protected String resetPassword(String userName) {
        String password =randomPassword.generateRandomPassword();
        userInterfaceObj.resetUserPassword(userName, password);
        return password;
    }


    // TODO should be done in a separate class
   //private String RandomPassword() {}

    protected String  disableUser(String userName ) {
        if(!userInterfaceObj.disableUserName(userName)) {
            return "user already inactive";
        }

        return "User "+userName+"  now is inactive";

    }

    protected String  enableUser(String userName) {

/*
        if(Objects.isNull(userName)){
            throw  new IllegalArgumentException ("Please enter user-name first ");

        }else if() {
            throw new IllegalArgumentException("This user-name does not exist");

        }
        */

        if(userInterfaceObj.enableUserName(userName)){

             return "User "+userName+" Now is Active";
        }

         return "user already active" ;


    }


    public String addNewUser(String userInformation) { //public void addNewUser(int userChoose) {
        //invalidUserChoose(userChoose);//No need
        String[] newUser = userInformation.split(",");
        if(newUser.length>3|| newUser.length<3){   ////////////////////////////////////////!!!!!!!!!!!!!!!!!!!!!!!!!
            throw new ArrayIndexOutOfBoundsException(" Please enter semicolon between each\ninput and another(name , user-name ,  e-mail ) and check you" +
                    "have enter three inputs\nno less no more which are : name , user-name ,  e-mail ");
            //You hava to enter 3 things : name , user-name ,  e-mail
        }
        String password=randomPassword.generateRandomPassword();
        UserData user = new UserData(newUser[0], newUser[1], newUser[2], password, false);
        userInterfaceObj.addNewUser(user);
        /*
         if(!userInterfaceObj.addNewUser(user))
            throw new IllegalArgumentException("User-name or email is alreay exist");
         */
        return password;
    }



    private void invalidUserChoose(int userChoose) {
        if (userChoose != 2) {
            throw new IllegalArgumentException("user Choose must be 2");
        }
    }





    public  String getUsersFromDB(){
        return userInterfaceObj.displayAllUsers();
    }

    /*
           <dependency>
            <groupId>email-validator</groupId>
            <artifactId>email-validator</artifactId>
            <version>0.1</version>
        </dependency>


     */

}

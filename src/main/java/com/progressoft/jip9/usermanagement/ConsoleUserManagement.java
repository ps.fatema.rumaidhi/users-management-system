package com.progressoft.jip9.usermanagement;

import javax.jws.soap.SOAPBinding;
import java.io.Console;
import java.util.ArrayList;
import java.util.Scanner;

public class ConsoleUserManagement{
    private UsersManagement usersManagement;

    ConsoleUserManagement(UsersManagement usersManagement){
        this.usersManagement = usersManagement;
    }

    public int promptUser() throws IllegalArgumentException {

        Scanner input = new Scanner(System.in);
        int userChoose, validateChoose;
        do {
            displayMenu();
            userChoose = input.nextInt();
            validateChoose = usersManagement.ValidateUserChoose(userChoose);
            SwitchValidateUserChoose(validateChoose);
        } while (userChoose != 6);
        return validateChoose;
    }

    protected void SwitchValidateUserChoose(int validateChoose) {
        switch (validateChoose) {
            case 1:
                displayAll();
                break;
            case 2:
                String userInformation= promptUserForUserData();
                String userPassword =usersManagement.addNewUser(userInformation);
                displayPassword(userPassword);


                break;
            case 3:
                String userName = enableUserPrompt();
                String userActiveStatus =usersManagement.enableUser(userName);
                display(userActiveStatus);
                break;
            case 4:
                display(usersManagement.disableUser(disableUserPrompt()));
                break;
            case 5:
                String user= resetPasswordPrompt();
               String password= usersManagement.resetPassword(user);
                displayNewPassword(password);

                break;
            default:
                display("Exist Program");
               usersManagement.exist();

        }
    }

    private String disableUserPrompt() {
        System.out.println("Enter userName you want to disable");
        Scanner userInput = new Scanner(System.in);
        return  userInput.nextLine();
    }

    private void  displayPassword(String password){
        System.out.println("Your Password is: " + password);
    }

    private void  displayNewPassword(String password){
        System.out.println("Your new Password is: " + password);
    }

    private String resetPasswordPrompt() {
        System.out.println("please enter your username to reset your password");
        Scanner userInput = new Scanner(System.in);
        return userInput.nextLine();
    }

    private String enableUserPrompt() {
        System.out.println("Enter user-name you want to enable");
        Scanner userInput = new Scanner(System.in);
        return userInput.nextLine(); /////////////////////////.tirm()
    }

    private void displayMenu() {
        System.out.print("\n1- Display all users\n2- Add new user\n" +
                "3- Enable user\n4- Disable user\n" +
                "5- Reset Password\n" +
                "6- Exit\nPlease enter your option:\n ");
    }



    private String promptUserForUserData() {
        System.out.println("Enter User Information in the format (user, name, email):");
        System.out.println("example:HR, Human resource, hr@ps.com");
        Scanner userInput = new Scanner(System.in);
        return userInput.nextLine().trim();

    }
    public void displayAll() {

        // TODO the process of printing is the same regardless of where the users are stored

        System.out.println("+-------------+-------------+-------------+------------------");
        System.out.println("| Username    |  Name       |   E-mail    |     Active(yes/no)");
        System.out.println("+-------------+-------------+-------------+----------------");
        String users= usersManagement.getUsersFromDB();
        System.out.println(users);
        System.out.println("+-------------+-------------+-------------+-------------");
    }


    public void display(String displayString){
        System.out.println(displayString);
    }

    public void userChangeStatus(String status){
        System.out.println("User status change to "+ status);
    }



}



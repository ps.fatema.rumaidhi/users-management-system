public interface UserInterface {
    void DisplayAllUsers();
   void addNewUser(UserData newUser);
   void enableUserName(String userName);
   void disableUserName(String userName);
   void resetUserPassword(String username,String newPassword);
   boolean validateUserAndEmail(String userName, String email);
   boolean IsExistUserNameOrEmail(String userName, String email, int i);
}

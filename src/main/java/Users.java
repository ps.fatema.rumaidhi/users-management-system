import java.util.ArrayList;

public class Users implements UserInterface

{


     private ArrayList<UserData> usersList = new ArrayList<UserData>();

    public  boolean validateUserAndEmail(String userName, String email) {
        for (int i = 0; i < usersList.size(); i++) {
            if (IsExistUserNameOrEmail(userName, email, i)) return false;
            /*
            if(userName.equals(usersList.get(i).username)){
                System.out.println("this user name is alrealy exist");
                return false;
            }
            if (email.equals(usersList.get(i).email)){
                System.out.println("this email is alrealy exist");
                return false;
            }
             */
        }
        return true;
    }

    public boolean IsExistUserNameOrEmail(String userName, String email, int i) {
        if (userName.equals(usersList.get(i).getUsername()) || email.equals(usersList.get(i).getEmail())) {
            System.out.println("this user name or  email is alrealy exist");
            return true;
        }
        return false;
    }

/*
    public void addNewUser(User newUser) {
        if (!validateUserAndEmail(newUser.username, newUser.email)) {

            throw new IllegalArgumentException("User-name or email is alreay exist");
        } else {
            usersList.add(newUser);
          //  System.out.println("elements No:" + usersList.size());
        }// Collections.sort(usersList);  // Sort
    }


 */
    public void addNewUser(UserData newUser) {
        if (!validateUserAndEmail(newUser.getName(), newUser.getEmail())) {

            throw new IllegalArgumentException("User-name or email is alreay exist");
        } else {
            usersList.add(newUser);
            //  System.out.println("elements No:" + usersList.size());
        }// Collections.sort(usersList);  // Sort
    }

    public  void enableUserName(String userName) {

        for (int i = 0; i < usersList.size(); i++) {

            if (userName.equals(usersList.get(i).getUsername())) {

                if (usersList.get(i).getActiveState() == true) {
                    System.out.println("user already active");
                } else {

                    usersList.get(i).setActive(true);
                }
            }
        }
    }




    public  void disableUserName(String userName) {
        for (int i = 0; i < usersList.size(); i++) {
            if (userName.equals(usersList.get(i).getUsername())) {
                if (usersList.get(i).getActiveState() == false) {
                    System.out.println("user already inactive");
                }
                    usersList.get(i).setActive(false);
            }
        }
    }


    public  void DisplayAllUsers() {
        if(usersList.size()==0){
           System.out.println("List is Empty");
            throw new IllegalArgumentException("List is Empty");

        }
        else {
            System.out.println("+-------------+-------------+-------------+------------------");
            System.out.println("| Username    |  Name       |   E-mail    |     Active(yes/no)");
            System.out.println("+-------------+-------------+-------------+----------------");
            for (int i = 0; i < usersList.size(); i++) {

                System.out.print("|        " + usersList.get(i).getUsername());
                System.out.print("    |         " + usersList.get(i).getName());
                System.out.print("   |         " + usersList.get(i).getEmail());

                if (!usersList.get(i).getActiveState()) {
                    System.out.println("   |       " + "No");
                } else {
                    System.out.println("   |       " + "Yes");
                }
                // System.out.print(" " + usersList.get(i).password);
            }
            System.out.println("+-------------+-------------+-------------+-------------");
        }
    }


    public   void resetUserPassword(String username,String newPassword){
       for (int i = 0; i < usersList.size(); i++) {
           if(username.equals(usersList.get(i).getUsername())){
              usersList.get(i).setPassword(newPassword);
           }
       }

   }
}
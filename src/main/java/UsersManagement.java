import org.apache.commons.lang3.RandomStringUtils;

import java.io.IOException;
import java.util.Scanner;

public class UsersManagement  {
    private  UserInterface userInterfaceObj;

    public UsersManagement(UserInterface user2){
        this.userInterfaceObj= user2;
    }
    public  int promptUser() throws IllegalArgumentException {

        Scanner input = new Scanner(System.in);
        int userChoose,validateChoose;
        do {
            DisplayMenu();
            userChoose = input.nextInt();
            validateChoose = ValidateUserChoose(userChoose);
            SwitchValidateUserChoose(validateChoose);
        } while (userChoose != 6);
        //  int userChoose=3;
        return validateChoose;
    }

    private   void SwitchValidateUserChoose(int validateChoose) {
        switch (validateChoose){
            case 1: userInterfaceObj.DisplayAllUsers();break;
            case 2: addNewUser(validateChoose);break;
            case 3: enableUser();break;
            case 4: disableUser();break;
            case 5: resetPassword();break;
            default:System.exit(0);
        }
    }

    private  void DisplayMenu() {
        System.out.print("1- Display all users\n2- Add new user\n" +
                "3- Enable user\n4- Disable user\n" +
                "5- Reset Password\n" +
                "6- Exit\nPlease enter your option:\n ");
    }

    private void resetPassword() {
        System.out.println("please enter your username to reset your password");
        Scanner userInput = new Scanner(System.in);
        String userName=userInput.nextLine();
        String password = RandomPassword();
        System.out.println(password);
        userInterfaceObj.resetUserPassword(userName,password);
        System.out.println("Your new Password is: "+password);
    }

    private  String RandomPassword() {
        String lettersFirstgroup= RandomStringUtils.randomAlphabetic(2);
        String lettersSecondgroup= RandomStringUtils.randomAlphabetic(1);
        String  numbersFirstgroup= RandomStringUtils.randomNumeric(2);
        String  numberslastgroup= RandomStringUtils.randomNumeric(3);
        return lettersFirstgroup+numbersFirstgroup+lettersSecondgroup+numberslastgroup;
            /* Random r = new Random();char c = (char)(r.nextInt(26) + 'a'); */
    }

    private  void disableUser() {
        System.out.println("Enter userName you want to disable");
        Scanner userInput = new Scanner(System.in);
        String userName=userInput.nextLine();
        userInterfaceObj.disableUserName(userName);
    }

    private  void enableUser() {
        System.out.println("Enter userName you want to enable");
        Scanner userInput = new Scanner(System.in);
        String userName=userInput.nextLine();
        userInterfaceObj.enableUserName(userName);
    }




    public  void addNewUser(int userChoose) {
        invalidUserChoose(userChoose);
        String userInformation = PromptUser();
        String [] newUser= userInformation.split(" ");
        String password = RandomPassword();
        UserData user= new UserData(newUser[0],newUser[1],newUser[2],password,false);
        System.out.println("Your new  password is:"+password);
        userInterfaceObj.addNewUser(user);
    }

    private  String PromptUser() {
        System.out.println("Enter User Information in the format (user, name, email):");
        System.out.println("example:HR, Human resource, hr@ps.com");
        Scanner userInput = new Scanner(System.in);
        return userInput.nextLine();
    }

    private   void invalidUserChoose(int userChoose) {
        if(userChoose != 2 ){
            throw  new IllegalArgumentException("user Choose must be 2");
        }
    }

    protected  int ValidateUserChoose(int userChoose) throws IllegalArgumentException {
        boolean trueChoose = userChooseWithinTheRange(userChoose);
        if (!trueChoose) {
            throw new IllegalArgumentException("Please enter a number from 1 - 6 only");
        } else {
            return userChoose;
        }
    }

    private  boolean userChooseWithinTheRange(int userChoose) {
        int[] chooses = {1, 2, 3, 4, 5, 6};
        boolean trueChoose = false;
        for (int index = 0; index < 6; index++) {
            if (userChoose == chooses[index]) trueChoose = true;
        }
        return trueChoose;
    }



    /*
           <dependency>
            <groupId>email-validator</groupId>
            <artifactId>email-validator</artifactId>
            <version>0.1</version>
        </dependency>


     */

}

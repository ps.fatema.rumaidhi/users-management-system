package com.progressoft.jip9.usermanagement;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.jws.soap.SOAPBinding;
import java.io.IOException;

public class UsersManagementTest {                                          //do all the tests

    @Test
    public void givenInValidNumber_whenPromptUser_thenThrowsIllegalArgumentException(){
        DataBase db = new ArrayListDataBase();
        UsersManagement usersManagement= new UsersManagement(db);
        int userChoose=9;
      IllegalArgumentException exception=Assertions.assertThrows(IllegalArgumentException.class
              ,()->usersManagement.ValidateUserChoose(userChoose));
      Assertions.assertEquals("Please enter a number from 1 - 6 only",exception.getMessage());
    }

    @Test
    public void givenletterInsteadOfNumber_whenPromptUser_thenThrowsIllegalArgumentException(){
        //No need : nextInt throws InputMismatchException
    }

    @Test
    public void givenValidNumber_whenPromptUser_thenReturnChooseNumberIsValid(){
        int actualChoose= 5;//UsersManagement.promptUser();   /////////////////
        int expectedChoose=1;
        boolean userChooseExisting= false;
        int [] menuNumbers ={1,2,3,4,5,6};

        for (int menueNumber = 0; menueNumber <6 ; menueNumber++) {
            if(actualChoose ==menuNumbers[menueNumber]) {
                expectedChoose = actualChoose;                                                   //!!!!!!!!!!!!
                break;
            }
        }

        Assertions.assertEquals(expectedChoose,actualChoose);

    }




    //!!!!Not in UserMang ,coz Not ask user
    @Test
    public void givenEmptyArrayList_whenDispllayAllUsers_thenThrowsIllegalArgumentException(){
        DataBase db = new ArrayListDataBase();
        UsersManagement usersManagement= new UsersManagement(db);
       Exception ex=Assertions.assertThrows(IllegalArgumentException.class,()->usersManagement.displayAllUsers());
       Assertions.assertEquals("List is Empty",ex.getMessage());

    }




    @Test
    public void givenAlreayExistUser_whenAddNewUser_thenThrowsIllegalArgumentException(){
        DataBase db_ArrayList= new ArrayListDataBase();
        UsersManagement usersManagement= new UsersManagement(db_ArrayList);

        usersManagement.addNewUser("HR, Human resource, hr@ps.com");      ///!!!!!!!!!!!!!!!!
        IllegalArgumentException exception=Assertions.assertThrows(IllegalArgumentException.class,()->usersManagement.addNewUser("HR, Human resource, hr@ps.com"));
        Assertions.assertEquals("User-name or email is alreay exist",exception.getMessage());
    }

    @Test
    public void givenUserInputWithOutSemicolon_whenAddNewUser_thenThrowsArrayIndexOutOfBoundsException(){
        DataBase db_ArrayList= new ArrayListDataBase();
        UsersManagement usersManagement= new UsersManagement(db_ArrayList);
        IndexOutOfBoundsException exception=Assertions.assertThrows(IndexOutOfBoundsException.class,()->usersManagement.addNewUser("HR   Human resource  hr@ps.com"));
        Assertions.assertEquals(" Please enter semicolon between each input and another(name , user-name ,  e-mail ) and check you" +
                "have enter three inputs no less no more which are : name , user-name ,  e-mail ",exception.getMessage());

    }

    @Test
    public void givenMoreNumberOfUserInput_whenAddNewUser_thenThrowsArrayIndexOutOfBoundsException(){
        //more input
        DataBase db_ArrayList= new ArrayListDataBase();
        UsersManagement usersManagement= new UsersManagement(db_ArrayList);
        IndexOutOfBoundsException exception=Assertions.assertThrows(IndexOutOfBoundsException.class,()->usersManagement.addNewUser("Nora, HR, Nora , hr@ps.com"));
        Assertions.assertEquals(" Please enter semicolon between each input and another(name , user-name ,  e-mail ) and check you" +
                "have enter three inputs no less no more which are : name , user-name ,  e-mail ",exception.getMessage());
    }


      @Test
    public void givenLessNumberOfInputs_whenAddNewUser_thenThrowsArrayIndexOutOfBoundsException(){
   //less  input
          DataBase db_ArrayList= new ArrayListDataBase();
          UsersManagement usersManagement= new UsersManagement(db_ArrayList);
          IndexOutOfBoundsException exception=Assertions.assertThrows(IndexOutOfBoundsException.class,()->usersManagement.addNewUser("HR,  hr@ps.com"));
          Assertions.assertEquals(" Please enter semicolon between each\ninput and another(name , user-name ,  e-mail ) and check you" +
                  "have enter three inputs\nno less no more which are : name , user-name ,  e-mail ",exception.getMessage());

    }



    @Test
    public void givenVaildUserInput_whenAddNewUser_thenAddNewUserToArrayList(){

        //!!!!!!!!!!!!Printp(%s - 12)
    }

    @Test
    public void givenInvalidUserName_whenEnableUser_thenThrowsIllegalArgumentException(){
     //Invalid Or Unexist UserName in DB
        DataBase db_arrayList= new ArrayListDataBase();
        UsersManagement usersManagement=new UsersManagement(db_arrayList);
        IllegalArgumentException exception=Assertions.assertThrows(IllegalArgumentException.class,()->usersManagement.enableUser("o"));
        //expected is any store data in the ArrayListDataBase //!!!!!!
        Assertions.assertEquals("Invaild user-name",exception.getMessage());
        



    }

/*
    @Test
    public void givenEmptyUserName_whenEnableUser_thenThrowsIllegalArgumentException(){
        //end-user did not specify any userName, he press enter only

        DataBase db_ArrayList= new ArrayListDataBase();
        UsersManagement usersManagement= new UsersManagement(db_ArrayList);
        IndexOutOfBoundsException exception=Assertions.assertThrows(IndexOutOfBoundsException.class,()->usersManagement.addNewUser("Nora, HR, Nora , hr@ps.com"));
        Assertions.assertEquals(" Please enter semicolon between each input and another(name , user-name ,  e-mail ) and check you" +
                "have enter three inputs no less no more which are : name , user-name ,  e-mail ",exception.getMessage());
    }


  @Test
    public void givenEnableUser_whenEnableUser_thenThrowsIllegalArgumentException(){
        DataBase db_arrayList= new ArrayListDataBase();
        UsersManagement usersManagement=new UsersManagement(db_arrayList);
        usersManagement.enableUser("Adnan");

        IllegalArgumentException exception= Assertions.assertThrows(IllegalArgumentException.class,()->usersManagement.enableUser("Adnan"));
        Assertions.assertEquals("user already active",exception.getMessage());
    }

    @Test
    public void givenMoreNumberOfUserInput_whenEnableUser_thenThrowsIllegalArgumentException(){
        //more user input

    }
    */
    @Test
    public void givenValidUserName_whenEnableUser_thenEnableUserInTheArrayList(){

    }

/*
    @Test
    public void givenDisableUser_whenDisableUser_thenThrowsIllegalArgumentException(){

        IllegalArgumentException exception= Assertions.assertThrows(IllegalArgumentException.class,()->UsersManagement.addNewUser(3));
        Assertions.assertEquals("User already inactive”",exception.getMessage());
    }
    */

    @Test
    public void givenInvalidUserName_whenDisableUser_thenThrowsIllegalArgumentException(){
        //Invalid Or Unexist UserName in DB
        DataBase db_arrayList= new ArrayListDataBase();
        UsersManagement usersManagement=new UsersManagement(db_arrayList);
        IllegalArgumentException exception=Assertions.assertThrows(IllegalArgumentException.class,()->usersManagement.disableUser("o"));
        //expected is any store data in the ArrayListDataBase

        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Assertions.assertEquals("Invaild user-name",exception.getMessage());



    }


    @Test
    public void givenEmptyUserName_whenDisableUser_thenThrowsIllegalArgumentException(){
        //end-user did not specify any userName, he press enter only (the program work well now!!)
    }


    @Test
    public void givenMoreNumberOfUserInput_whenDisableUser_thenThrowsIllegalArgumentException(){
        //more user input

    }
    @Test
    public void givenValidUserName_whenDisableUser_thenDisableUserInTheArrayList(){

    }


    @Test
    public void givenInvalidUserName_whenResetPassword_thenThrowsIllegalArgumentException(){
        //more user input

    }
    @Test
    public void givenMoreNumberOfUserInput_whenResetPassword_thenThrowsIllegalArgumentException(){
        //more user input

    }
    @Test
    public void givenValidUserName_whenResetPassword_thenResetUserPassword(){

    }





}
